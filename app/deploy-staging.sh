#!/bin/bash

cd /home/gitlab-runner/sot-edrans/app
sed -i "s/registry.gitlab.com/nicrom/sot-edrans-app:$1/g" docker-compose.override.yml
docker-compose up -d
